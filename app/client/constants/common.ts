export const TEST = {
  desc: 'hello world',
}

// layout settings
export const CONTENT_WIDTH_DESKTOP = 1170;
export const CONTENT_WIDTH_MOBILE = 327;

// breakpoints
export const BREAKPOINTS_MOBILE = 480;
export const BREAKPOINTS_TABLE = 1024;

