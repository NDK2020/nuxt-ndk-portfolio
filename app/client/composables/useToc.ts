import {ParsedContent} from "@nuxt/content/dist/runtime/types";
import {Ref} from "nuxt/dist/app/compat/vue-demi";

export const useToc = (post:Ref<Pick<ParsedContent,string>>) => {
  if (!post) return [];
  let p = toRaw(unref(post));
  // console.log("useToc: ", p)
  return computed(() => {
    const items = p.excerpt?.children
    if (!items) return [];
    const toc = [];
    const tags = ["h2", "h3", "h4", "h5", "h6"]
    items.forEach((item) => {
      if (tags.includes(item.tag)) {
        toc.push({
          id: item.props.id,
          // reg /\d$/ mean The line ends with a number
          title: item.props.id.toString().replace(/-/g, " ").replace(/\d$/, " "),
          depth: Number(item.tag.replace(/h/g, ""))
        })
      }
    })
    return toc;
  })
}
