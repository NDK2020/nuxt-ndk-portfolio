import {Post} from "../types/post";

export const MOCK_POSTS:Post[] = [
  {
    category: "Web",
    title: "hello",
    thumbnail: "assets/images/works/1.svg",
    route: "posts"
  },
  {
    category: "Web",
    title: "hello",
    thumbnail: "assets/images/works/2.svg",
    route: {name: "posts"}
  },
  {
    category: "Web",
    title: "hello",
    thumbnail: "assets/images/works/3.svg",
    route: {name: "posts"}
  },
  {
    category: "Blog",
    title: "hello",
    thumbnail: "assets/images/works/4.svg",
    route: {name: "posts"}
  },
  {
    category: "Blog",
    title: "hello",
    thumbnail: "assets/images/works/5.svg",
    route: {name: "posts"}
  },
  {
    category: "Blog",
    title: "hello",
    thumbnail: "assets/images/works/6.svg",
    route: {name: "posts"}
  },
];
