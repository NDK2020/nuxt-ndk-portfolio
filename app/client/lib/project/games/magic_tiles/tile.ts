import p5 from "p5";
import {BREAKPOINTS_MOBILE} from "~~/client/constants";

class Base {
  p5: p5;
  constructor(p5: p5) {
    this.p5 = p5;
  }
}

export class Tile extends Base {
  id: string;
  w: number;
  h: number;
  x: number;
  y: number;
  velocity: number;
  isClicked: boolean;
  isPressing: boolean;
  isPressed: boolean;
  pressRec: {
    x: number;
    y: number;
    w: number;
    h: number;
  };
  pressArc: {
    x: number;
    y: number;
    w: number;
    h: number;
    start: number;
    stop: number;
  };
  pressColor: string
  isEnded: boolean

  constructor(p5: p5, x: number, y: number, h: number, v: number) {
    super(p5);
    this.w = 40;
    this.h = h;
    this.x = x;
    this.y = y;
    this.velocity = v;
    this.isClicked = false;
    this.isPressing = false;
    this.isPressed = false;
    //this.pressColor = this.randomColor('');
    this.pressColor = '#fae';
    this.id = Date.now().toString(36);
    this.pressRec = {
      x: this.x,
      y: 0,
      w: 0,
      h: 0,
    };
    this.pressArc = {
      x: this.x + this.w / 2,
      y: 0,
      w: 0, 
      h: 0,
      start: -p5.HALF_PI,
      stop: 0,
    }
    this.isEnded = false;
    if (this.p5.windowWidth <= BREAKPOINTS_MOBILE) {
      this.w = 30;
    }
  }
  show() {
    let fill = this.isClicked ? "grey" : "black";
    this.update();
    this.p5.push();
    this.p5.translate(this.x, this.y);
    this.p5.fill(fill);
    // this.p5.rectMode(this.p5.CENTER);
    this.p5.rect(0, 0, this.w, this.h);
    this.p5.pop();

    if (!this.isPressing) return;
    if (this.isPressed) {
      let rate = this.isEnded ? 0 : 1;
      this.pressRec.y += rate;
      this.pressArc.y += rate;
    } 
    if (this.pressColor === 'noFill') 
      this.p5.noFill();
    else
      this.p5.fill(this.pressColor);

    this.p5.rect(
      this.pressRec.x,
      this.pressRec.y,
      this.pressRec.w,
      this.pressRec.h
    );
    this.p5.arc(
      this.pressArc.x,
      this.pressArc.y,
      this.pressArc.w,
      this.pressArc.h,
      this.pressArc.start,
      this.pressArc.stop
    );
  }
  update() {
    this.y += this.isEnded ? 0 : this.velocity;
  }

  randomColor() {
    return this.p5.color(this.p5.random(255), this.p5.random(255), this.p5.random(255));
  }
}
