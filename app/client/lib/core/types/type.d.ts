declare type HttpResponse<T = undefined> = {
  data?: T
  message?: string
  error?: string
  statusCode?: number
  pagination?: { [k: string]: number }
  detail?: {
    property: string
    message: [string]
  }[]
}
