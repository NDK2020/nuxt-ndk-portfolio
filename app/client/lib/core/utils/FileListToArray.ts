export const fileListToArray = (files: FileList) => {
  const res: File[] = []
  for (let i = 0; i < files.length; i++) {
    let file = files.item(i)
    if (file) {
      res.push(file)
    }
  }
  return res
}
