export const assertResponse = (res?: HttpResponse<any>): boolean => {
    if (!res) return false;
    return Math.floor(Number(res.statusCode) / 100) === 2
}
 
export const convertErrorMessage = (m?: string, codeEntities = "Code") => {
    if (!m) return ""
 
    if (m.includes("code") && m.includes("already") 
        && m.includes("exists"))
            return `${codeEntities} already exists`
 
    if (m.includes("newPassword") && m.includes("does not match")) 
            return "The new password and password confirmation do not match."
 
    return m
}
 
export const cloneObjectsInArr = (arr: any) => {
    let tmp:any = []
    arr.forEach ((item: any)  => {
        tmp.push({...item})
    })
    return tmp;
}
 
export const removeDuplicateObjsInArray = <T>(arr: T[]) => {
 
    let checkedIdList: number[] = []
    let clone: T[] = [];
    let key: keyof T
    arr.forEach((x, xId) => {
        if (checkedIdList.indexOf(xId) === -1) {
            clone.push(x);
            checkedIdList.push(xId)
        }
        arr.forEach((y, yId) => {
            let isEqual = true;
            if (checkedIdList.indexOf(yId) === -1) {
                for (key in y) {
                    if (x[key] !== y[key])
                    isEqual = false; 
                }
            }
            // 
            if (isEqual) checkedIdList.push(yId)
        })
    })
 
    return clone;
}
 
// String manipulation
//
//
export const formatPaymentMethod = (method: string) => {
    return method === "cards" ? "Mastercard" : "Other Method";
}
 
export const formatPhoneNumber = (phone: string) => {
    return "+1" + " (202) " + phone; 
}
 
export const formatMenuTitle = (url: string) => {
    let formatTitle = "";
    if (url) {
        formatTitle = url.substring(url.lastIndexOf('/') + 1) 
        formatTitle = upperCaseFirstLetter(formatTitle)
    }
    if (url === "/") formatTitle = "Dashboard"
    return formatTitle
}
 
export const getFirstWordAfterSlashInUrl = () => {
    //eslint-disable-next-line
    return window.location.pathname.replace(/^\/([^\/]*).*$/, '$1');
}
export const upperCaseFirstLetter = (word: string) => {
    let res = "";
    res = word.charAt(0).toUpperCase() + word.slice(1)
    return res;
}
 
 
export const numberWithCommas = (x: number) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
 
 
export const getIdLastPart = (id: string) => {
    return id.substring( id.lastIndexOf('-') + 1)
}
