import {RouteLocationRaw} from "vue-router";

export type PostCategory = "Web" | "Blog";
export interface Post {
  category: PostCategory;
  title: string;
  thumbnail: string,
  route?: string | RouteLocationRaw
}

