---
title: "Hỷ Lâm Môn"
short: "short desc"
slug: "hylammon"
category: "Web"
thumbnail: "/assets/images/posts/hylammon/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["hylammon"] } }
---

# Hỷ Lâm Môn

::VisitSiteButton
---
text: 'Xem trang'
href: 'http://hylammon.com.vn'
---
::

Hỷ Lâm Môn là tiệm bánh với hơn 30 năm kinh nghiệm tại thành phố Hồ Chí Minh.
Website mới của họ bao gồm các sản phẩm mới, và tiện lợi hơn cho nhân viên trong
việc cập nhật sản phẩm.

## Công nghệ sử dụng

::StackList
---
tagList: ['Wordpress', 'Elementor', 'Jquery', 'Php']
---
::

## Một số hình ảnh

::Swiper
---
slides: ['/assets/images/posts/hylammon/1.png', '/assets/images/posts/hylammon/2.png', '/assets/images/posts/hylammon/3.png', /assets/images/posts/hylammon/4.png]
---
::

