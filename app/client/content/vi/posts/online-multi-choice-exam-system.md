---
title: "Hệ thống thi trắc nghiệm"
short: "short desc"
slug: "online-multi-choice-exam-system"
category: "Web"
thumbnail: "/assets/images/posts/online-exam/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["online-multi-choice-exam-system"] } }
---

# Hệ thống thi trắc nghiệm 
::VisitSiteButton
---
text: 'to demo vids'
href: 'https://www.youtube.com/playlist?list=PLeGMjHOnmWw9soohJRWS4VGK_WJqAnJ88'
---
::
Đây là đồ án tốt nghiệp, sử dụng công nghệ React cho FE và NodeJs cho BE.

## Công nghệ sử dụng

::StackList
---
tagList: ['Typescript', 'React', 'Redux', 'BulmaCss', 'PostgresSQL', 'GraphQL',
'Docker', 'LetsEncryptSSL']
---
::

## Một số hình ảnh

::Swiper
---
slides: ['/assets/images/posts/online-exam/1.png', '/assets/images/posts/online-exam/2.png', '/assets/images/posts/online-exam/3.png', /assets/images/posts/online-exam/4.png]
---
::
