---
title: "Abode"
description: "abode"
short: 'short desc'
slug: 'abode'
category: 'Web'
thumbnail: "/assets/images/posts/abode/thumbnail.png"
route: { name: 'posts-slug', params: {slug: ['abode']} }
---

# Abode

::VisitSiteButton
---
text: 'Xem trang'
href: 'http://abode.grisfox.studio'
---
::
Abode là công ti bất động sản và xây dựng ở Úc. Họ yêu cầu một trang web  có thiết kế tinh gọn, đẹp mắt nhưng vẫn cung cấp đầy đủ thông tin.

## Công nghệ sử dụng

::StackList
---
tagList: [ 'Wordpress', 'Elementor' ]
---
::

## Một số hình ảnh
::Swiper
---
slides: ['/assets/images/posts/abode/1.png', '/assets/images/posts/abode/2.png', '/assets/images/posts/abode/3.png', '/assets/images/posts/abode/4.png']
---
::
