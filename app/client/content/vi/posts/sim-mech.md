---
title: "Về nghề cũ"
short: "short desc"
slug: "sim-mech"
category: "Blog"
thumbnail: "/assets/images/posts/sim-mech/thumbnail.jpg"
route: { name: "posts-slug", params: { slug: ["sim-mech"] } }
---

# Về nghề cũ

Trước khi chuyển sang lập trình viên, tôi đã có kinh nghiệm 5 năm làm việc trong
lĩnh vực cơ khí ở vai trò sau

## Kỹ thuật viên dựng mô hình CAD

Sử dụng phần mềm CAD để thiết kế các công cụ gá cho ngành Body-in-white, tạo bản
vẽ để gia công. Ngoài ra, tôi còn lập trình một số công cụ để hỗ trợ các thao
tác lặp lại nhàm chán 

### Công nghệ sử dụng

::StackList
---
tagList: ['Catia', 'NX', 'SpaceClaim', 'Python', 'Visual Basic']
---
::

### Một số hình ảnh

::Swiper
---
slides: [ '/assets/images/posts/sim-mech/biw1.png', '/assets/images/posts/sim-mech/biw2.png', '/assets/images/posts/sim-mech/biw3.jpg' ]
---
::

## Kĩ thuật viên ngành mô phỏng
Nhiệm vụ chính là dựng các mô hình phần tử hữu hạn phức tạp, bắt đầu bằng cách
tạo mô hình 3d từ bản vẽ thiết kế, sau đó tiến hình chia lưới theo tiêu chuẩn
của công ti rồi đưa vào phần mềm mô phỏng cơ khí để tính toán.

### Công nghệ sử dụng

::StackList
---
tagList: ['Ansys Workbench', 'Hypermesh', 'PamCrash', 'Akselos', 'Trelis',
'SpaceClaim', 'Python', 'Tcl']
---
::

### Một số hình ảnh

::Swiper
---
slideType: 'before-after'
slides: [ ['/assets/images/posts/sim-mech/1.jpg', '/assets/images/posts/sim-mech/2.png'], ['/assets/images/posts/sim-mech/3.jpg', '/assets/images/posts/sim-mech/4.png'] ]
---
::
