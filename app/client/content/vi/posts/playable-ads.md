---
title: "Playable Ads"
short: "short desc"
slug: "playable-ads"
category: "Game"
thumbnail: "/assets/images/posts/playable-ads/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["playable-ads"] } }
---

# Playable-ads

Here are some of my playable-ads. 

## Tech Stack

::StackList
---
tagList: ['C#/C-sharp', 'Cocos Creator', 'Unity', 'Luna Playwork']
---
::

## Screenshots

::Swiper
---
slides: ['/assets/images/posts/playable-ads/the-last-PAds.jpg']
---
::
