---
title: "Shey shop"
short: "short desc"
slug: "shey-shop"
category: "Web"
thumbnail: "/assets/images/posts/shey-shop/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["shey-shop"] } }
---

# Shey shop
::VisitSiteButton
---
text: 'Xem trang'
href: 'https://practice-sheyshop.herokuapp.com/'
---
::

Tôi thực hiện dự án này theo khóa học trên mạng, mục đích để cải thiện kĩ năng
sử dụng React và học về MongoDB

## Công nghệ sử dụng

::StackList
---
tagList: ['Typescript', 'React', 'Redux', 'Bootstrap', 'MongoDB', 'Mongoose',
'Express']
---
::

## Một số hình ảnh

::Swiper
---
slides: ['/assets/images/posts/shey-shop/1.png', /assets/images/posts/shey-shop/2.png]
---
::
