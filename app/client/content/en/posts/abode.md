---
title: "Abode"
description: "abode"
short: 'short desc'
slug: 'abode'
category: 'Web'
thumbnail: "/assets/images/posts/abode/thumbnail.png"
route: { name: 'posts-slug', params: {slug: ['abode']} }
---

# Abode

::VisitSiteButton
---
href: 'http://abode.grisfox.studio'
---
::
Abode is a full service property development management and construction company
in Australia. They require a minimal and sleek website  but also capable of providing important info. 

## Tech Stack

::StackList
---
tagList: [ 'Wordpress', 'Elementor' ]
---
::

## Screenshots
::Swiper
---
slides: ['/assets/images/posts/abode/1.png', '/assets/images/posts/abode/2.png', '/assets/images/posts/abode/3.png', '/assets/images/posts/abode/4.png']
---
::
