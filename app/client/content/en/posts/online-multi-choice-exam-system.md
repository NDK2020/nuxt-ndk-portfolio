---
title: "Online Exam System"
short: "short desc"
slug: "online-multi-choice-exam-system"
category: "Web"
thumbnail: "/assets/images/posts/online-exam/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["online-multi-choice-exam-system"] } }
---

# Online multi-choice exam system
::VisitSiteButton
---
text: 'to demo vids'
href: 'https://www.youtube.com/playlist?list=PLeGMjHOnmWw9soohJRWS4VGK_WJqAnJ88'
---
::
I've built this fullstack graduation project to learn **React** and **NodeJS**.

## Tech Stack

::StackList
---
tagList: ['Typescript', 'React', 'Redux', 'BulmaCss', 'PostgresSQL', 'GraphQL',
'Docker', 'LetsEncryptSSL']
---
::

## Screenshots

::Swiper
---
slides: ['/assets/images/posts/online-exam/1.png', '/assets/images/posts/online-exam/2.png', '/assets/images/posts/online-exam/3.png', /assets/images/posts/online-exam/4.png]
---
::
