---
title: "My previous career"
short: "short desc"
slug: "sim-mech"
category: "Blog"
thumbnail: "/assets/images/posts/sim-mech/thumbnail.jpg"
route: { name: "posts-slug", params: { slug: ["sim-mech"] } }
---

# My previous career

Before becoming a front end dev, I have several years working in mechanical field as 2 roles below.

## Body-In-White drafter

use CAD software to develop new concept for fixture tools, create drawing for
manufacturing process. I've also do some coding side project to automate boring task of mentioned software
using scripting language such as Python or VB

### Tech Stack

::StackList
---
tagList: ['Catia', 'NX', 'SpaceClaim', 'Python', 'Visual Basic']
---
::

### Screenshots

::Swiper
---
slides: [ '/assets/images/posts/sim-mech/biw1.png', '/assets/images/posts/sim-mech/biw2.png', '/assets/images/posts/sim-mech/biw3.jpg' ]
---
::

## CAE Operator

My main task is building FEM model from converting 2d drawing to 3d model, then making mesh and
import it into simulation software.

### Tech Stack

::StackList
---
tagList: ['Ansys Workbench', 'Hypermesh', 'PamCrash', 'Akselos', 'Trelis',
'SpaceClaim', 'Python', 'Tcl']
---
::

### Screenshots

::Swiper
---
slideType: 'before-after'
slides: [ ['/assets/images/posts/sim-mech/1.jpg', '/assets/images/posts/sim-mech/2.png'], ['/assets/images/posts/sim-mech/3.jpg', '/assets/images/posts/sim-mech/4.png'] ]
---
::
