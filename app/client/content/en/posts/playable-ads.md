---
title: "Playable Ads"
short: "short desc"
slug: "playable-ads"
category: "Game"
thumbnail: "/assets/images/posts/playable-ads/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["playable-ads"] } }
---

# Playable-ads

"Here are a few of my playable ads."

## Tech Stack

::StackList
---
tagList: ['C#/C-sharp', 'Cocos Creator', 'Unity', 'Luna Playwork']
---
::

## Demos

Open the links below in your device's browser (Chrome recommended), 
and enable your microphone or turn on the sound, as this is a rhythm game


### Cat-Dash

This PAd may needs to play on mobile browser for better experience

::VisitSiteButton
---
text: 'GCD-autoplay'
href: 'https://gcd-01-autoplay.netlify.app/'
---
::

<br/>

::VisitSiteButton
---
text: 'GCD-manual'
href: 'https://gcd-01-manual.netlify.app/'
---
::

### Dancing Road


#### DR Playable Demo 01
::VisitSiteButton
---
text: 'Dr-PAd-01'
href: 'https://dr-01.netlify.app/'
---
::


#### DR IEC Demo 02
Below is an IEC ad, primarily designed for autoplay, 
where the user only needs to tap occasionally to move to the next step

::VisitSiteButton
---
text: 'Dr-IEC-02'
href: 'https://dr-02.netlify.app/'
---
::


### Duet Birds


::VisitSiteButton
---
text: 'GDUB-autoplay'
href: 'https://gdub-01-autoplay.netlify.app/'
---
::

<br/>

::VisitSiteButton
---
text: 'GDUB-manual'
href: 'https://gdub-01-manual.netlify.app/'
---
::
            
### Duet Cats

::VisitSiteButton
---
text: 'GDUC-autoplay'
href: 'https://gduc-01-autoplay.netlify.app/'
---
::

<br/>

::VisitSiteButton
---
text: 'GDUC-manual'
href: 'https://gduc-01-manual.netlify.app/'
---
::


### Duet Friends


::VisitSiteButton
---
text: 'GDUF-autoplay'
href: 'https://gduf-01-autoplay.netlify.app/'
---
::

<br/>

::VisitSiteButton
---
text: 'GDUF-manual'
href: 'https://gduf-01-manual.netlify.app/'
---
::


### Game music night battle

another FNF game

::VisitSiteButton
---
text: 'GMNB-autoplay'
href: 'https://gmnb-01-auto.netlify.app/'
---
::

<br/>


::VisitSiteButton
---
text: 'GMNB-manual'
href: 'https://gmnb-01-manual.netlify.app/'
---
::



### Match 3

A simple Match3 Fake Ad for TileHops

::VisitSiteButton
---
text: 'Match3xTileHop'
href: 'https://match3-th.netlify.app/'
---
::


### Magic Tiles 3

::VisitSiteButton
---
text: 'MT3-01-autoplay'
href: 'https://mt3-01-autoplay.netlify.app/'
---
::

<br/>

::VisitSiteButton
---
text: 'MT3-01-manual'
href: 'https://mt3-01-manual.netlify.app/'
---
::



### Tiles Hop 

::VisitSiteButton
---
text: 'TH-01-autoplay'
href: 'https://tiles-hop-01-autoplay.netlify.app/'
---
::

<br/>

::VisitSiteButton
---
text: 'TH-01-manual'
href: 'https://tiles-hop-01-manual.netlify.app/'
---
::




## Screenshots

::Swiper
---
slides: ['/assets/images/posts/playable-ads/1.jpg']
---
::
