---
title: "Shey shop"
short: "short desc"
slug: "shey-shop"
category: "Web"
thumbnail: "/assets/images/posts/shey-shop/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["shey-shop"] } }
---

# Shey shop
::VisitSiteButton
---
href: 'https://practice-sheyshop.herokuapp.com/'
---
::

I've built this hobby project following online video courses to enhance **React** skill and learn **MongoDB**.

## Tech Stack

::StackList
---
tagList: ['Typescript', 'React', 'Redux', 'Bootstrap', 'MongoDB', 'Mongoose',
'Express']
---
::

## Screenshots

::Swiper
---
slides: ['/assets/images/posts/shey-shop/1.png', /assets/images/posts/shey-shop/2.png]
---
::
