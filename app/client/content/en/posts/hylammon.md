---
title: "Hỷ Lâm Môn"
short: "short desc"
slug: "hylammon"
category: "Web"
thumbnail: "/assets/images/posts/hylammon/thumbnail.png"
route: { name: "posts-slug", params: { slug: ["hylammon"] } }
---

# Hỷ Lâm Môn

::VisitSiteButton
---
href: 'http://hylammon.com.vn'
---
::

Hỷ Lâm Môn is a HCM-based bakery with nearly 30 years of experience.
Their new website feature their product lines prominently, and is much easier
for staff to update and maintain.

## Tech Stack

::StackList
---
tagList: ['Wordpress', 'Elementor', 'Jquery', 'Php']
---
::

## Screenshots

::Swiper
---
slides: ['/assets/images/posts/hylammon/1.png', '/assets/images/posts/hylammon/2.png', '/assets/images/posts/hylammon/3.png', /assets/images/posts/hylammon/4.png]
---
::
