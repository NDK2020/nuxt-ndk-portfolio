import p5 from "p5";
import { Engine, Bodies, Constraint, World } from "matter-js";

export class Base {
  body: Matter.Body;
  p5: p5;
  constructor(p5: p5) {
    this.p5 = p5;
  }
}

export class BaseRect extends Base {
  w: number;
  h: number;

  constructor(p5: p5, w: number, h: number) {
    super(p5);
    this.w = w;
    this.h = h;
  }
  show() {
    const pos = this.body.position;
    const angle = this.body.angle;
    this.p5.push();
    this.p5.translate(pos.x, pos.y);
    this.p5.rotate(angle);
    this.p5.fill(255);
    this.p5.rectMode(this.p5.CENTER);
    this.p5.rect(0, 0, this.w, this.h);
    this.p5.pop();
  }
}

export class Box extends BaseRect {
  constructor(
    p5: p5,
    x: number,
    y: number,
    w: number,
    h: number,
    options?: { [key: string]: any }
  ) {
    super(p5, w, h);
    this.body = Bodies.rectangle(x, y, w, h, { isStatic: false, ...options });
  }
}

export class Ground extends Box {
  constructor(p5: p5, x: number, y: number, w: number, h: number) {
    super(p5, x, y, w, h);
    this.body.isStatic = true;
  }
}

export class BaseCircle extends Base {
  r: number;
  constructor(p5: p5, r: number) {
    super(p5);
    this.r = r;
  }
  show() {
    const pos = this.body.position;
    const angle = this.body.angle;
    this.p5.push();
    this.p5.translate(pos.x, pos.y);
    this.p5.rotate(angle);
    this.p5.fill(127);
    this.p5.strokeWeight(1);
    this.p5.stroke(255);
    this.p5.rectMode(this.p5.CENTER);
    this.p5.circle(0, 0, this.r * 2);
    // this.p5.ellipse(0, 0, this.r );
    this.p5.pop();
  }
}

export class Bird extends BaseCircle {
  constructor(p5: p5, x: number, y: number, r: number) {
    super(p5, r);
    this.body = Bodies.circle(x, y, r, { isStatic: true });
  }
}

export class SlingShot {
  world: Matter.World;
  sling: Matter.Constraint;
  p5: p5;
  constructor(
    p5: p5,
    world: Matter.World,
    x: number,
    y: number,
    body: Matter.Body
  ) {
    this.p5 = p5;
    const options = {
      pointA: { x: x, y: y },
      bodyB: body,
      stiffness: 0.05,
      length: 30,
    };
    this.sling = Constraint.create(options);
    World.add(world, this.sling);
  }

  show() {
    if (this.sling.bodyB) {
      this.p5.stroke(255);
      const posA = this.sling.pointA;
      const posB = this.sling.bodyB.position;
      this.p5.line(posA.x, posA.y, posB.x, posB.y);
    }
  }

  fly() {
    this.sling.bodyB = null;
  }

  attach(body: Matter.Body) {
    this.sling.bodyB = body;
  }
}
