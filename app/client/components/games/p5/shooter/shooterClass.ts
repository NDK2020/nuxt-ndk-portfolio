import p5 from "p5";
import { Body, Engine, Bodies, Constraint, World } from "matter-js";

export class Cannon {
  bullet: Matter.Body;
  world: Matter.World;
  p5: p5;
  x: number;
  y: number;
  w: number;
  h: number;
  r: number;
  speed: number;
  vec: p5.Vector;
  vmouse: p5.Vector;
  cannon: p5.Image;
  constructor(
    p5: p5,
    img: p5.Image,
    world: Matter.World,
    x: number,
    y: number,
    w = 60,
    h = 60
  ) {
    this.p5 = p5;
    this.cannon = img;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.vec = p5.createVector(x, y - h);
    this.speed = this.speed;
    this.vmouse = p5.createVector(p5.mouseX - this.x, p5.mouseY - this.y);
    this.r = 10;

    if (this.p5.windowWidth <= 480) {
      this.r = 5;
    }

    this.world = world;
    this.initBullet();
  }
  show() {
    const posX = this.bullet.position.x;
    const posY = this.bullet.position.y;
    const angle = this.bullet.angle;
    // if (this.checkIsMoving()) {
    //   this.initBullet();
    //   console.log("bullet: stopppp");
    // }
    // if (this.checkOutBound()) {
    //   console.log("bullet: outofbounds");
    //   this.initBullet();
    // }
    this.p5.push();
    this.p5.rectMode(this.p5.CENTER);
    this.p5.translate(posX, posY);
    this.p5.rotate(angle);
    //draw bullet
    this.p5.fill(127);
    this.p5.strokeWeight(1);
    this.p5.stroke(255);
    this.p5.circle(0, 0, this.r * 2);
    this.p5.pop();

    ///
    this.p5.push();
    let baseVec = this.p5.createVector(1, 0);
    this.p5.imageMode(this.p5.CENTER);
    this.p5.translate(this.x, this.y);
    this.p5.angleMode(this.p5.DEGREES);
    this.vmouse = this.p5.createVector(
      this.p5.mouseX - this.x,
      this.p5.mouseY - this.y
    );
    this.p5.rotate(baseVec.angleBetween(this.vmouse));
    this.p5.image(this.cannon, 0, 0);
    this.p5.pop();
  }
  shot() {
    Body.setStatic(this.bullet, false);
    this.vmouse = this.p5.createVector(
      this.p5.mouseX - this.x,
      this.p5.mouseY - this.y
    );
    this.vmouse.normalize();
    let pos = {
      x: this.bullet.position.x,
      y: this.bullet.position.y,
    };
    let force = {
      x: this.vmouse.x * 0.02,
      y: this.vmouse.y * 0.02,
    };
    if (this.p5.windowWidth <= 480) {
      force.x = this.vmouse.x * 0.003 
      force.y = this.vmouse.y * 0.003 
    }
    Body.applyForce(this.bullet, pos, force);
  }
  checkIsMoving() {
    return (
      this.cmp(this.bullet.velocity.x, 0) === 0 &&
      this.cmp(this.bullet.velocity.y, 0) === 0
    );
  }
  checkOutBound() {
    const x = this.bullet.position.x;
    const y = this.bullet.position.y;
    if (x > this.p5.width || x < 0) return true;
    if (y > this.p5.height || y < 0) return true;
    return false;
  }
  checkAtInitPos() {
    const posX = this.bullet.position.x;
    const posY = this.bullet.position.y;
    // let isAtX = this.cmp(posX, this.p5.width/2);
    // let isAtY = this.cmp(posY, this.p5.height - this.cannon.width);
    let isAtX = this.cmp(posX, this.x);
    let isAtY = this.cmp(posY, this.y);
    return isAtX === 0 && isAtY === 0;
  }
  cmp(x: number, y: number) {
    const EPS = 1e-9;
    return x < y - EPS ? -1 : x > y + EPS ? 1 : 0;
  }
  initBullet() {
    if (this.bullet) {
      World.remove(this.world, this.bullet);
    }
    // this.bullet = Bodies.circle(
    //   this.p5.width / 2,
    //   this.p5.height - this.cannon.width,
    //   this.r,
    //   { isStatic: true }
    // );
    this.bullet = Bodies.circle(this.x, this.y, this.r, { isStatic: true });
    World.add(this.world, this.bullet);
  }
}

class Base {
  body: Matter.Body;
  p5: p5;
  constructor(p5: p5) {
    this.p5 = p5;
  }
}

export class TextBox extends Base {
  onGround: boolean;
  w: number;
  h: number;
  constructor(
    p5: p5,
    x: number,
    y: number,
    w: number,
    h: number,
    options?: { [key: string]: any }
  ) {
    super(p5);
    this.w = w;
    this.h = h;
    this.onGround = false;
    this.body = Bodies.rectangle(x, y, w, h, { isStatic: false, ...options });
  }
  show(char: string, wordWidth: number, fontSize: number, font?: any) {
    const pos = this.body.position;
    const angle = this.body.angle;
    //

    if (font) this.p5.textFont(font);
    this.p5.push();
    this.p5.translate(pos.x, pos.y);
    this.p5.rotate(angle);
    this.p5.rectMode(this.p5.CENTER);
    this.p5.fill(255, 0, 0, 0);
    this.p5.rect(0, 0, wordWidth, fontSize);

    this.p5.fill(255);
    this.p5.textAlign(this.p5.CENTER);
    this.p5.text(char, 0, fontSize / 2);
    this.p5.pop();
  }
}
