import { defineNuxtConfig } from "nuxt";
import { IntlifyModuleOptions } from "@intlify/nuxt3";
import Components from "unplugin-vue-components/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";

declare module "@nuxt/schema" {
  interface NuxtConfig {
    intlify?: IntlifyModuleOptions;
  }
}

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  app: {
    head: {
      titleTemplate: "ndkhoa - %s",
      htmlAttrs: {
        lang: "en",
      },
      meta: [
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        {
          hid: "description",
          name: "description",
          content: "Nuxt 3 Awesome Starter",
        },
      ],
      link: [{ rel: "icon", type: "image/png", href: "/favicon.png" }],
    },
  },
  // plugins: [
  //   {src: "~/plugins/vue-writer", ssr: false}
  // ],
  srcDir: "client/",
  components: {
    global: true,
    dirs: [
      "~/components/atoms",
      "~/components/molecules",
      // "~/components/games/tetris",
      "~/components/games/p5",
      "~/lib/components",
      "~/content",
      "~/components",
    ],
  },
  css: [
    "ant-design-vue/dist/antd.css",
    /* "virtual:windi-base.css",
    "virtual:windi-components.css",
    "virtual:windi-utilities.css",
    "mdi/css/materialdesignicons.min.css", */
  ],

  buildModules: [
    /* 'nuxt-windicss', */
    "@unocss/nuxt",
  ],

  modules: [
    "@pinia/nuxt",
    "@intlify/nuxt3",
    "@vueuse/motion/nuxt",
    "@vueuse/nuxt",
    "@nuxt/content",
  ],

  build: {
    transpile: ["gsap"],
  },

  content: {
    highlight: {
      theme: "dracula-soft",
    },
  },

  intlify: {
    localeDir: "lib/project/locales",
    vueI18n: {
      locale: "en",
      fallbackLocale: "en",
      availableLocales: ["en", "vi"],
    },
  },
  //  experimental: {
  //   reactivityTransform: true,
  //   viteNode: true,
  // }, 
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/scss/index.scss";',
        },
      },
    },
    plugins: [
      Components({
        resolvers: [AntDesignVueResolver({ resolveIcons: true })],
      }),
    ],
    // @ts-expect-error: Missing ssr key
    ssr: {
      noExternal: ["moment", "compute-scroll-into-view", "ant-design-vue"],
    },
  },
});
